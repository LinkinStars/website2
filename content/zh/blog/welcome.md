---
title: Xorm博客定型
description: ""
lead: "采用新的博客站点生成工具"
date: 2015-10-01T06:00:00+08:00
lastmod: 2020-11-04T09:19:42+01:00
draft: false
weight: 10
contributors: ["Lunny Xiao"]
tags: ["go", "golang", "xorm", "orm", "sql", "database"]
---

终于迎来了个人认为最有发展潜力的静态站点生成工具 [Pugo](https://github.com/go-xiaohei/pugo)发布了第一个稳定版本，本博客将使用其搭建，将会发布关于xorm的高质量文章和新版本等信息.
