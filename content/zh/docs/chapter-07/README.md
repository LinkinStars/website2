---
title: "删除数据"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-07"
weight: 4100
toc: true
---

## 删除数据

删除数据用 `Delete`方法，参数为struct的指针并且成为查询条件。

```Go
user := new(User)
affected, err := engine.ID(id).Delete(user)
```

`Delete`的返回值第一个参数为删除的记录数，第二个参数为错误。

注意1：当删除时，如果user中包含有bool,float64或者float32类型，有可能会使删除失败。具体请查看 <a href="#160">FAQ</a>
注意2：必须至少包含一个条件才能够进行删除，这意味着直接用

```Go
engine.Delete(new(User))
```

将会报一个保护性的错误，如果你真的希望将整个表删除，你可以

```Go
engine.Where("1=1").Delete(new(User))
```