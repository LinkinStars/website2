---
title: "Get one record"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-05"
weight: 6300
toc: true
---

### 5.3.Get one record
Fetch a single object by user

```Go
var user = User{ID:27}
has, err := engine.Get(&user)
// or has, err := engine.ID(27).Get(&user)

var user = User{Name:"xlw"}
has, err := engine.Get(&user)
```
